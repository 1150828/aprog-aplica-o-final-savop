package aprog.savop;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Métodos utilitários.
 */
public class Utilitarios {

    public static Scanner in = new Scanner(System.in);

    /**
     * Imprime o menu e lê a opção escolhida, fazendo a verificação
     * da mesma.
     * Retorna a opção escolhida.
     */
    public static int menu() {
        int escolha;
        String resposta;
        String menu = "\nMenu:"
                + "\n1: Ler o ficheiro com a informação dos Deputados"
                + "\n2: Visualizar informação sobre todos os Deputados"
                + "\n3: Alterar informação de um Deputado"
                + "\n4: Ler ficheiro com a votação pretendida"
                + "\n5: Visualizar informações e votos de todos os Deputados"
                + "\n6: Visualizar e guardar Resultados da votação"
                + "\n7: Visualizar Resultados segundo uma faixa etária"
                + "\n8: Criar página HTML com os Resultados da votação"
                + "\n0: Sair do programa"
                + "\n"
                + "\nEscolha uma opção: ";

        // Repete o menu até ser selecionado um inteiro válido.
        do {
            System.out.print(menu);
            resposta = in.nextLine();
            if (isDigit(resposta)) {
                escolha = Integer.parseInt(resposta);
            } else {
                escolha = -1;
            }
        } while (escolha > 8 || escolha < 0);
        return escolha;
    }

    /**
     * Menu de alteração dos dados do deputado
     * Retorna a escolha.
     *
     * @param deputado - id do deputado
     */
    public static int menuDadosDeputado(String[] deputado) {
        int escolha;
        String resposta;
        System.out.println("");
        Utilitarios.cabecalho();
        System.out.printf("%-6s| %-30s| %-10s| %-12s%n", deputado[0], deputado[1], deputado[2], deputado[3]);
        String menu = "\nATUALIZAR INFORMAÇÃO"
                + "\n1: Nome"
                + "\n2: Data de Nascimento"
                + "\n0: Sair"
                + "\n\nEscolha uma opção: ";
        // Repete o menu até ser selecionado um inteiro válido.
        do {
            System.out.print(menu);
            resposta = in.nextLine();
            if (isDigit(resposta)) {
                escolha = Integer.parseInt(resposta);
            } else {
                escolha = -1;
            }
        } while (escolha > 2 || escolha < 0);
        return escolha;
    }

    /**
     * Gera um cabeçalho com id, nome, partido e data de nascimento
     */
    public static void cabecalho() {
        System.out.printf("%-6s| %-30s| %-10s| %-12s%n", "ID", "NOME", "PARTIDO", "DATA NASC");
    }

    /**
     * Gera um cabeçalho com id, nome, partido e voto
     */
    public static void cabecalhoVoto() {
        System.out.printf("%-6s| %-20s| %-10s| %-12s%n", "ID", "NOME", "PARTIDO", "VOTO");
    }

    /**
     * Cria uma pausa na consola para permitir que o utilizador 
     * prossiga quando estiver pronto
     */
    public static void pausa() {
        System.out.printf("%n%n%s%n", "Clique [ENTER] para continuar");
        in.nextLine();
    }

    /**
     * Cria uma mensagem de erro na consola e de seguida cria uma pausa
     * 
     * @param erro - Mensagem de erro a mostrar ao utilizador
     */
    public static void erro(String erro) {
        System.out.println("\n\nErro:");
        System.out.println(erro);
        pausa();
    }

    /**
     * Pesquisa o deputado por id na matriz deputados.
     * Retorna a posição caso exista e -1 caso não exista.
     * 
     * @param idDeputado - identificação do deputado a procurar
     * @param nDeputados - número de deputados na matriz deputados
     * @param deputados - matriz dos deputados
     */
    public static int pesquisarDeputadoPorID(String idDeputado, int nDeputados, String[][] deputados) {
        for (int i = 0; i < nDeputados; i++) {
            if (deputados[i][0].equalsIgnoreCase(idDeputado)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Verifica se uma string é um digito.
     * Retorna verdadeiro ou falso com o resultado.
     * 
     * @param string - string a verificar
     */
    public static boolean isDigit(String string){
        char c = string.charAt(0);
        return (c > 47 && c < 58);
    }

    /**
     * Ordena a matriz dos votos.
     * 
     * @param votos - matriz dos votos
     * @param nVotos - número de votos
     */
    public static void ordenarVotos(String[][] votos, int nVotos) {
        for (int i = 0; i < nVotos; i++) {
            for (int j = 0; j < nVotos; j++) {
                int comparacao = votos[i][0].compareTo(votos[j][0]);
                if (comparacao < 0) {
                    trocarDados(i, j, votos, votos[i].length);
                }
            }
        }
    }

    /**
     * Seleciona o primeiro e ultimo nome dum deputado.
     * Retorna uma string do tipo: "Nome Apelido"
     * 
     * @param deputados - matriz dos deputados
     * @param pos - posição do deputado na matriz
     */
    public static String nomeCurto(String[][] deputados, int pos){
        String[] nomes = deputados[pos][1].split(" ");
        if (nomes.length == 1) {
            return nomes[0];
        } else {
            return nomes[0] + " " + nomes[nomes.length-1];
        }
    }

    /**
     * Adiciona resultado à matriz partidos, caso já exista o partido
     * ao qual se está a tentar adicioanr, soma apenas, caso ainda não
     * exista, é adicionada uma nova linha com esse partido.
     * Retorna o número total de partidos.
     * 
     * @param partido - partido do deputado que votou
     * @param voto - voto (Sim, Não ou Absteve-se)
     * @param partidos - matriz dos partidos e resultados
     * @param nPartidos - número de partidos na matriz
     */
    public static int adicionarResultado(String partido, String voto, String[][] partidos, int nPartidos) {
        // Para todos os partidos já existentes
        for (int i = 0; i < nPartidos; i++) {
            if (partidos[i][0].equalsIgnoreCase(partido)) {
                if (voto.equalsIgnoreCase("s")) {
                    partidos[i][1]=String.valueOf(Integer.parseInt(partidos[i][1])+1);
                } else if (voto.equalsIgnoreCase("n")) {
                    partidos[i][2]=String.valueOf(Integer.parseInt(partidos[i][2])+1);
                } else if (voto.equalsIgnoreCase("a")) {
                    partidos[i][3]=String.valueOf(Integer.parseInt(partidos[i][3])+1);
                }
                partidos[i][4]=String.valueOf(Integer.parseInt(partidos[i][4])+1);
                
                return nPartidos;
            }
        }
        
        // Se chegar aqui cria novo partido visto ainda não existir
        partidos[nPartidos][0]=partido;
        partidos[nPartidos][1]="0";
        partidos[nPartidos][2]="0";
        partidos[nPartidos][3]="0";
        partidos[nPartidos][4]="1"; // 1 deputado
        
        if (voto.equalsIgnoreCase("s")) {
            partidos[nPartidos][1]="1";
        } else if (voto.equalsIgnoreCase("n")) {
            partidos[nPartidos][2]="1";
        } else if (voto.equalsIgnoreCase("a")) {
            partidos[nPartidos][3]="1";
        }
        
        nPartidos++;
        return nPartidos;
        
    }

    /**
     * Ordena os partidos por número de deputados,
     * caso o número seja igual, ordena alfabeticamente pelo
     * nome do partido.
     * 
     * @param partidos - matriz dos partidos e resultados
     * @param nPartidos - número de partidos na matriz
     */
    public static void ordenarPartidos(String[][] partidos, int nPartidos) {
        for (int i = 0; i < nPartidos; i++) {
            for (int j = 0; j < nPartidos; j++) {
                if (Integer.parseInt(partidos[i][4]) > Integer.parseInt(partidos[j][4])) {
                    trocarDados(i, j, partidos, partidos[i].length);
                } else if(Integer.parseInt(partidos[i][4]) == Integer.parseInt(partidos[j][4])) {
                    if (partidos[j][0].compareTo(partidos[i][0]) > -1) {
                        trocarDados(i, j, partidos, partidos[i].length);
                    }
                }
            }
        }  
    }

    /**
     * Troca a posição da linha i com a j na matriz recebida.
     * 
     * @param i - posição 1
     * @param j - posição 2
     * @param matriz - matriz recebida
     * @param colunas - número de colunas na matriz
     */
    public static void trocarDados(int i, int j, String[][] matriz, int colunas) {
        String temp;
        for (int k = 0; k < colunas; k++) {
            temp = matriz[i][k];
            matriz[i][k] = matriz[j][k];
            matriz[j][k] = temp;
        }
    }

    /**
     * Calcula e retorna a idade a partir da data de nascimento recebida (Ex: 19850321 -> 30 anos).
     * 
     * @param dataNasc - data de nascimento do deputado
     */
    public static int calcularIdade(String dataNasc) {
        
        GregorianCalendar dataAtual = new GregorianCalendar();
        int anoAtual = dataAtual.get(Calendar.YEAR),
            mesAtual = dataAtual.get(Calendar.MONTH) + 1, // Pois janeiro corresponde ao valor 0
            diaAtual = dataAtual.get(Calendar.DAY_OF_MONTH),
            anoNasc = 0,
            mesNasc = 0,
            diaNasc = 0;
        
        if (dataNasc != null && !dataNasc.equalsIgnoreCase("")) {
            anoNasc = Integer.parseInt(dataNasc.substring(0, 4).trim());
            mesNasc = Integer.parseInt(dataNasc.substring(4, 5).trim());
            diaNasc = Integer.parseInt(dataNasc.substring(6).trim());
        }
        
        int idade = anoAtual - anoNasc;
        
        if (mesAtual < mesNasc) {
            idade--;
        } else if (mesAtual == mesNasc && diaAtual < diaNasc) {
            idade--;
        }
        
        return idade;
    }
}
