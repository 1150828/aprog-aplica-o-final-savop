package aprog.savop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 * Classe principal e métodos principais
 */
public class Main {

    /**
     * Constantes da aplicação
     */
    private final static int MAX_DEPUTADOS = 230;
    private final static File FILE_DEPUTADOS = new File("Deputados.txt");
    private final static String PAGINA_HTML = "Resultado.html";
    private final static int MAX_LINHAS_PAGINA = 3;
    
    public static Scanner in = new Scanner(System.in);

    /**
     * Cria um menu em loop até ser escolhida a opção zero.
     */
    public static void main(String[] args) throws FileNotFoundException {

        String[][] deputados = new String[MAX_DEPUTADOS][4];
        String[][] votos = new String[MAX_DEPUTADOS][2];
        String[][] partidos = new String[MAX_DEPUTADOS][5];
        int nDeputados = 0;
        int nVotos = 0;
        int nPartidos = 0;
        String lei = "";

        int op;
        do {
            op = Utilitarios.menu();
            switch (op) {
                case 1:
                    nDeputados = lerFicheiro(deputados, nDeputados);
                    break;
                case 2:
                    listagemPaginada(deputados, nDeputados);
                    break;
                case 3:
                    System.out.print("\nID do deputado que deseja alterar: ");
                    String id = in.nextLine();
                    actualizaInfoDeputado(id, deputados, nDeputados);
                    break;
                case 4:  
                    System.out.print("\nNome da lei que deseja ler (Sem .txt): ");
                    lei = in.nextLine();
                    nVotos = lerFicheiroVotos(votos, nVotos, lei);
                    break;
                case 5:
                    visualizarVotos(votos, nVotos, deputados, nDeputados);
                    break;
                case 6:  
                    nPartidos = resultadoVotos(deputados, nDeputados, votos, nVotos, partidos, nPartidos, lei);
                    break;
                case 7:  resultadoIdade(deputados, nDeputados, votos, nVotos, lei);
                    break;
                case 8:  gerarHTML(partidos, nPartidos, lei);
                    break;
                default: // Sair
                    break;
            }
        } while (op != 0);
    }

    /**
     * Lê ficheiro dos deputados linha a linha e preenche a
     * matriz deputados com a informação lida.
     * Retorna o número de deputados.
     *
     * @param deputados - matriz dos deputados
     * @param nDeputados - número de deputados na matriz
     */
    private static int lerFicheiro(String[][] deputados, int nDeputados) throws FileNotFoundException {
        Scanner lerFicheiro = new Scanner(FILE_DEPUTADOS);
        while (lerFicheiro.hasNextLine()) {
            String linha = lerFicheiro.nextLine();
            if (linha != null && !linha.equals("")) {
                String[] campos = linha.split(";");
                for (int i = 0; i < campos.length; i++) {
                    deputados[nDeputados][i] = campos[i].trim();
                }
                nDeputados++;
            }
        }
        lerFicheiro.close();
        return nDeputados;
    }

    /**
     * Imprime para o utilizador a lista de todos os deputados
     * e das suas informações.
     *
     * @param deputados - matriz dos deputados
     * @param nDeputados - número de deputados na matriz
     */
    private static void listagemPaginada(String[][] deputados, int nDeputados) {
        if (nDeputados < 1) {
            Utilitarios.erro("Primeiro deve ler o ficheiro");
        } else {
            int pagAtual = 0;
            for (int i = 0; i < nDeputados; i++) {
                if (i % MAX_LINHAS_PAGINA == 0) {
                    if (pagAtual > 0) {
                        Utilitarios.pausa();
                    }
                    pagAtual++;
                    System.out.println("\n" + pagAtual + "ª PÁGINA");
                    Utilitarios.cabecalho();
                }
                System.out.printf("%-6s| %-30s| %-10s| %-12s%n", deputados[i][0],
                        deputados[i][1], deputados[i][2], deputados[i][3]);
            }
            Utilitarios.pausa();
        }
    }
    
    /**
     * Método que permite alterar a informação (nome e data de nascimento)
     * de qualquer deputados a partir do ID.
     * Retorna verdadeiro ou falso se o deputado for encontrado ou não.
     *
     * @param idDeputado - identificação do deputado
     * @param deputados - matriz dos deputados
     * @param nDeputados - número de deputados na matriz
     */
    private static boolean actualizaInfoDeputado(String idDeputado, String[][] deputados, int nDeputados) {
        int pos;
        if ((pos = Utilitarios.pesquisarDeputadoPorID(idDeputado, nDeputados, deputados)) > -1) {
            System.out.printf("");
            int op;
            do {
                op = Utilitarios.menuDadosDeputado(deputados[pos]);
                switch (op) {
                    case 1:
                        System.out.print("Novo nome: ");
                        deputados[pos][1] = in.nextLine();
                        break;
                    case 2:
                        System.out.print("Nova data (Ex: AAAAMMDD): ");
                        deputados[pos][3] = in.nextLine();
                        break;
                    case 0:
                        System.out.println("Informação do deputado editada");
                        break;
                    default:
                        System.out.println("Opção incorreta");
                        break;
                }
            } while (op != 0);
        } else {
            Utilitarios.erro("O deputado " + idDeputado + " não foi encontrado!");
            return false;
        }
        return true;
    }

    /**
     * Lê o ficheiro da lei que o utilizador inseriu e introduz
     * todos os votos para a matriz votos, retornando posteriormente
     * o número de votos total.
     *
     * @param votos - matriz com todos os votos
     * @param nVotos - número de votos na matriz
     * @param lei - nome da lei lida
     */
    private static int lerFicheiroVotos(String[][] votos, int nVotos, String lei) throws FileNotFoundException {
        File ficheiro = new File(lei + ".txt");
        Scanner lerFicheiro = new Scanner(ficheiro);
        while (lerFicheiro.hasNextLine()) {
            String linha = lerFicheiro.nextLine();
            if (linha != null && !linha.equals("")) {
                String id = linha.substring(0, 5);
                String voto = linha.substring(5);
                votos[nVotos][0] = id;
                votos[nVotos][1] = voto;
                nVotos++;
            }
        }
        lerFicheiro.close();
        Utilitarios.ordenarVotos(votos, nVotos);
        return nVotos; // Número de deputados
    }

    /**
     * Mostra por paginação e ordenado alfabeticamente pelo id
     * todos os votos da lei lida no ponto 4.
     *
     * @param deputados - matriz com todos os deputados
     * @param nDeputados - número de deputados na matriz
     * @param votos - matriz com todos os votos
     * @param nVotos - número de votos na matriz
     */
    private static void visualizarVotos(String[][] votos, int nVotos, String[][] deputados, int nDeputados) {
        if (nVotos < 1) {
            Utilitarios.erro("Primeiro deve ler o ficheiro");
        } else {
            int pagAtual = 0;
            for (int i = 0; i < nVotos; i++) {
                int pos = Utilitarios.pesquisarDeputadoPorID(votos[i][0], nDeputados, deputados);
                if (pos > -1) {
                    if (i % MAX_LINHAS_PAGINA == 0) {
                        if (pagAtual > 0) {
                            Utilitarios.pausa();
                        }
                        pagAtual++;
                        System.out.println("\n" + pagAtual + "ª PÁGINA");
                        Utilitarios.cabecalhoVoto();
                    }
                    System.out.printf("%-6s| %-20s| %-10s| %-12s%n", votos[i][0], Utilitarios.nomeCurto(deputados, pos), deputados[pos][2], votos[i][1]);
                }
            }
            Utilitarios.pausa();
        }
    }

    /**
     * Cria um ficheiro com os resultados dos votos de cada partido
     * ordenado por total de deputados e caso seja o mesmo ordenar
     * alfabeticamente, mostra também na consola o resultado.
     * Retorna o número de partidos total
     *
     * @param deputados - matriz com todos os deputados
     * @param nDeputados - número de deputados na matriz
     * @param votos - matriz com todos os votos
     * @param nVotos - número de votos na matriz
     * @param partidos - matriz com todos os partidos
     * @param npartidos - número de partidos na matriz
     * @param lei - nome da lei lida anteriormente
     */
    private static int resultadoVotos(String[][] deputados, int nDeputados, String[][] votos, int nVotos, String[][] partidos, int nPartidos, String lei) throws FileNotFoundException {
        
        // Criar ficheiro
        File ficheiroResultados = new File("Resultados_" + lei + ".txt");
        Formatter resultadosFile = new Formatter(ficheiroResultados);
        
        for (int i = 0; i < nVotos; i++) {
            int pos = Utilitarios.pesquisarDeputadoPorID(votos[i][0], nDeputados, deputados);
            nPartidos = Utilitarios.adicionarResultado(deputados[pos][2], votos[i][1], partidos, nPartidos);
        }
        Utilitarios.ordenarPartidos(partidos, nPartidos);
        int totalS=0, totalN=0, totalA=0;
        System.out.println("\n\nVotação de: " + lei);
        resultadosFile.format("Votação de: %s%n", lei);
        for (int i = 0; i < nPartidos; i++) {
            System.out.printf("%n%-10s Votos a favor: %-3s Votos contra: %-3s Abstenções: %-3s", partidos[i][0] + ";", partidos[i][1] + ";", partidos[i][2] + ";", partidos[i][3] + ".");
            resultadosFile.format("%n%-10s Votos a favor: %-3s Votos contra: %-3s Abstenções: %-3s", partidos[i][0] + ";", partidos[i][1] + ";", partidos[i][2] + ";", partidos[i][3] + ".");
            totalS+=Integer.parseInt(partidos[i][1]);
            totalN+=Integer.parseInt(partidos[i][2]);
            totalA+=Integer.parseInt(partidos[i][3]);
        }
        System.out.printf("%n%n%-10s Votos a favor: %-3s Votos contra: %-3s Abstenções: %-3s", "Totais:", totalS + ";", totalN + ";", totalA + ".");
        resultadosFile.format("%n%n%-10s Votos a favor: %-3s Votos contra: %-3s Abstenções: %-3s%n", "Totais:", totalS + ";", totalN + ";", totalA + ".");
        
        resultadosFile.close();
        
        Utilitarios.pausa();
        return nPartidos;
    }

    /**
     * Gera a página html com os resultados obtidos no ponto 6
     *
     * @param partidos - matriz com todos os partidos
     * @param npartidos - número de partidos na matriz
     * @param lei - nome da lei lida anteriormente
     */
    private static void gerarHTML(String[][] partidos, int nPartidos, String lei) throws FileNotFoundException {
        
        // Criar página html
        File ficheiro = new File(PAGINA_HTML);
        Formatter pagHTML = new Formatter(ficheiro);
        
        PaginaHTML.iniciarPagina(pagHTML, "Resultados " + lei);
        PaginaHTML.cabecalho(pagHTML, 1, "Votação de " + lei);
        String[] titulos = {"Partido", "Votos a Favor", "Votos Contra", "Abstenções"};
        
        PaginaHTML.criarTabelaComLinhaTitulos(pagHTML, titulos, partidos, nPartidos, 4);
        
        PaginaHTML.fecharPaginaComData(pagHTML);
        
        pagHTML.close();
    }

    /**
     * Mostra resultados da votação da lei de certa faixa etária à
     * escolha do utilizador.
     * Mostra faixas de idades:
     * -menores ou igual a 35
     * -maiores que 35 e menores que 60
     * -maiores ou igual a 60
     * 
     * @param deputados - matriz com todos os deputados
     * @param nDeputados - número de deputados na matriz
     * @param votos - matriz com todos os votos
     * @param nVotos - número de votos na matriz
     * @param lei - nome da lei lida anteriormente
     */
    private static void resultadoIdade(String[][] deputados, int nDeputados, String[][] votos, int nVotos, String lei) {
        /*
            linha 0 = votos menores ou iguais a 35 anos
            linha 1 = votos entre 35 e 60 anos
            linha 2 = votos maiores ou iguais a 60 anos
            coluna 0 = votos a favor
            coluna 1 = votos contra
            coluna 2 = abstenções
        */
        int[][] contagem = new int[3][3];
        
        for (int i = 0; i < nVotos; i++) {
            int pos = Utilitarios.pesquisarDeputadoPorID(votos[i][0], nDeputados, deputados);
            int idade = Utilitarios.calcularIdade(deputados[pos][3]);
            
            int posCont = -1;
            
            if (idade >= 60) {
                posCont = 2;
            } else if (idade <= 35) {
                posCont = 0;
            } else {
                posCont = 1;
            }
            
            if (votos[i][1].equalsIgnoreCase("s")) {
                contagem[posCont][0]++;
            } else if (votos[i][1].equalsIgnoreCase("n")) {
                contagem[posCont][1]++;
            } else if (votos[i][1].equalsIgnoreCase("a")) {
                contagem[posCont][2]++;
            }
            
        }
        System.out.printf("%n%nVotação por faixa etária de %s%n", lei);
        System.out.printf("%nIdades menores ou iguais a 35 --> Percentagem a Favor: %s; Percentagem Contra %s; Percentagem Abstenções: %s.", (double)(Double.parseDouble(""+contagem[0][0])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[0][1])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[0][2])/nVotos*100) + "%");
        System.out.printf("%nIdades entre 35 e 60 --> Percentagem a Favor: %s; Percentagem Contra %s; Percentagem Abstenções: %s.", (double)(Double.parseDouble(""+contagem[1][0])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[1][1])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[0][2])/nVotos*100) + "%");
        System.out.printf("%nIdades superiores ou iguais a 60 --> Percentagem a Favor: %s; Percentagem Contra %s; Percentagem Abstenções: %s.", (double)(Double.parseDouble(""+contagem[2][0])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[2][1])/nVotos*100) + "%", (double)(Double.parseDouble(""+contagem[2][2])/nVotos*100) + "%");
        Utilitarios.pausa();
    }
}
